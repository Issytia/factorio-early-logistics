local tint = {
	provider = { r = 0.761, g = 0.314, b = 0.318, a = 1 },
	storage = { r = 0.788, g = 0.643, b = 0.322, a = 1 },
	vehicle = { r = 0.373, g = 0.588, b = 0.745, a = 1 }
}

local item = table.deepcopy(data.raw.item["stone-brick"])
item.name = "early-logistics-provider"
item.icons = { { icon = item.icon, tint = tint.provider } }
item.icon = nil
item.order = item.order .. "-a[early-logistics-provider]"
item.place_as_tile.result = item.name

local tile = table.deepcopy(data.raw.tile["stone-path"])
tile.name = item.name
tile.order = tile.order .. "-a[early-logistics-provider]"
tile.minable.result = item.name
tile.tint = tint.provider
tile.layer = 250
if settings.startup["issy-tile-clear"].value then
	tile.decorative_removal_probability = 1
end

local recipe = {
	type = "recipe",
	name = item.name,
	enabled = true,
	ingredients = { { "stone-brick", 1 } },
	result = item.name
}

data:extend({ item, tile, recipe })

item = table.deepcopy(data.raw.item["stone-brick"])
item.name = "early-logistics-storage"
item.icons = { { icon = item.icon, tint = tint.storage } }
item.icon = nil
item.order = item.order .. "-a[early-logistics-storage]"
item.place_as_tile.result = item.name

tile = table.deepcopy(data.raw.tile["stone-path"])
tile.name = item.name
tile.order = tile.order .. "-a[early-logistics-storage]"
tile.minable.result = item.name
tile.tint = tint.storage
tile.layer = 250
if settings.startup["issy-tile-clear"].value then
	tile.decorative_removal_probability = 1
end

recipe = {
	type = "recipe",
	name = item.name,
	enabled = true,
	ingredients = { { "stone-brick", 1 } },
	result = item.name
}

data:extend({ item, tile, recipe })

item = table.deepcopy(data.raw.item["stone-brick"])
item.name = "early-logistics-vehicle"
item.icons = { { icon = item.icon, tint = tint.vehicle } }
item.icon = nil
item.order = item.order .. "-a[early-logistics-vehicle"
item.place_as_tile.result = item.name

tile = table.deepcopy(data.raw.tile["stone-path"])
tile.name = item.name
tile.order = tile.order .. "-a[early-logistics-vehicle]"
tile.minable.result = item.name
tile.tint = tint.vehicle
tile.layer = 250
if settings.startup["issy-tile-clear"].value then
	tile.decorative_removal_probability = 1
end

recipe = {
	type = "recipe",
	name = item.name,
	enabled = true,
	ingredients = { { "stone-brick", 1 } },
	result = item.name
}

data:extend({ item, tile, recipe })
