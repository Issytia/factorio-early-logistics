require("util")
local v = require('semver')

local function find_chests(e, range)
	local chests, seen = {}, {}

	for _, tile in pairs(e.surface.find_tiles_filtered {
		position = e.position,
		radius = range,
		name = {
			"early-logistics-provider",
			"early-logistics-storage"
		},
		to_be_deconstructed = false
	}) do
		local pos = { x = tile.position.x + 0.5, y = tile.position.y + 0.5 }
		for _, chest in pairs(e.surface.find_entities_filtered {
			position = pos,
			type = { "container", "logistic-container" },
			to_be_deconstructed = false
		}) do
			if seen[chest] == nil then
				seen[chest] = true

				if e.force.is_friend(chest.force) then
					table.insert(chests, {
						storage = tile.name == "early-logistics-storage",
						pos = chest.position,
						inv = chest.get_inventory(defines.inventory.chest)
					})
				end
			end
		end
	end

	for _, chest in pairs(e.surface.find_entities_filtered {
		position = e.position,
		radius = range,
		type = { "logistic-container" },
		to_be_deconstructed = false
	}) do
		if seen[chest] == nil then
			seen[chest] = true

			local mode = game.entity_prototypes[chest.name].logistic_mode

			if mode ~= "requester" and e.force.is_friend(chest.force) then
				table.insert(chests, {
					storage = mode == "storage",
					pos = chest.position,
					inv = chest.get_inventory(defines.inventory.chest)
				})
			end
		end
	end

	return chests
end

local function find_chests_player(p)
	if p.character == nil then return {} end

	local range = settings.global["issy-el-range"].value
	if range == 0 then range = p.reach_distance end

	return find_chests(p, range)
end

local function find_chests_vehicle(v)
	if v.force.players[1] == nil then return {} end

	local range = settings.global["issy-el-range"].value
	if range == 0 then range = v.force.players[1].reach_distance end

	return find_chests(v, range)
end

local function find_vehicles()
	if table_size(global.vehicle_tiles) == 0 then return {} end

	local vehicles, seen = {}, {}

	for surface_index, tiles in pairs(global.vehicle_tiles) do
		local surface = game.get_surface(surface_index)
		for pos, _ in pairs(tiles) do
			pos = { x = pos.x + 0.5, y = pos.y + 0.5 }
			for _, vehicle in pairs(surface.find_entities_filtered {
				position = pos,
				type = { "car" },
				to_be_deconstructed = false
			}) do
				if seen[vehicle.unit_number] == nil then
					seen[vehicle.unit_number] = true

					table.insert(vehicles, vehicle)
				end
			end
		end
	end

	return vehicles
end

local function rescan_tiles()
	global.vehicle_tiles = {}

	for _, surface in pairs(game.surfaces) do
		for _, tile in pairs(surface.find_tiles_filtered {
			name = {
				"early-logistics-vehicle"
			}
		}) do
			if global.vehicle_tiles[surface.index] == nil then global.vehicle_tiles[surface.index] = {} end
			global.vehicle_tiles[surface.index][tile.position] = true
		end
	end
end

script.on_event({
	defines.events.script_raised_set_tiles,
	defines.events.on_player_built_tile,
	defines.events.on_player_mined_tile,
	defines.events.on_robot_built_tile,
	defines.events.on_robot_mined_tile
}, function(e)
	if global.vehicle_tiles[e.surface_index] == nil then global.vehicle_tiles[e.surface_index] = {} end

	for _, t in pairs(e.tiles) do
		if e.tile then -- built_tile
			if e.tile.name == "early-logistics-vehicle" then
				global.vehicle_tiles[e.surface_index][t.position] = true
			elseif t.old_tile.name == "early-logistics-vehicle" then
				global.vehicle_tiles[e.surface_index][t.position] = nil
			end
		elseif t.old_tile then -- mined_tile
			if t.old_tile.name == "early-logistics-vehicle" then
				global.vehicle_tiles[e.surface_index][t.position] = nil
			end
		else -- set_tiles
			if t.name == "early-logistics-vehicle" then
				global.vehicle_tiles[e.surface_index][t.position] = true
			else
				global.vehicle_tiles[e.surface_index][t.position] = nil
			end
		end
	end
end)

local function float_text(p, pos, item, count)
	if p == nil then return end
	p.surface.create_entity {
		name = "flying-text",
		position = pos,
		text = "[item=" .. item .. "] " .. count,
		color = count < 0 and { r = 0.8, g = 0.1, b = 0.1, a = 1 } or { r = 0.1, g = 0.8, b = 0.1, a = 1 },
		render_player_index = p.index
	}
end

local function is_construction_bot(name)
	local construction_bots = game.get_filtered_item_prototypes { {
		filter = "place-result",
		elem_filters = { { filter = "type", type = "construction-robot" } }
	} }

	is_construction_bot = function(name)
		return construction_bots[name] ~= nil
	end

	return is_construction_bot(name)
end

local function handle_player(p)
	if p.character == nil or
			(settings.get_player_settings(p)["issy-el-use-checkbox"].value and
				not p.character_personal_logistic_requests_enabled)
	then
		return
	end

	local chests

	local trash = p.get_inventory(defines.inventory.character_trash)

	for i = 1, p.character.request_slot_count do
		local req = p.get_personal_logistic_slot(i)
		if req.min > 0 then
			local wanted = req.min - p.get_item_count(req.name)
			if p.character.logistic_cell ~= nil and p.character.logistic_cell.logistic_network ~= nil then
				local network = p.character.logistic_cell.logistic_network
				if is_construction_bot(req.name) then
					wanted = req.min - network.all_construction_robots
				elseif game.item_prototypes[req.name].type == "repair-tool" then
					for _, bot in pairs(network.construction_robots) do
						wanted = wanted - bot.get_inventory(defines.inventory.robot_repair).get_item_count(req.name)
					end
				end
			end

			if wanted > 0 then
				wanted = math.min(wanted, game.item_prototypes[req.name].stack_size)

				local in_trash = trash.get_item_count(req.name)
				if in_trash > 0 then
					local found = math.min(wanted, in_trash)

					if p.can_insert({ name = req.name, count = found }) then
						local inserted = p.insert({ name = req.name, count = found })
						float_text(p, p.position, req.name, inserted)
						trash.remove({ name = req.name, count = inserted })
						return
					end
				else
					if chests == nil then chests = find_chests_player(p) end

					for j = 1, #chests do
						local found = math.min(wanted, chests[j].inv.get_item_count(req.name))
						if found > 0 and p.can_insert({ name = req.name, count = found }) then
							local inserted = p.insert({ name = req.name, count = found })
							float_text(p, chests[j].pos, req.name, inserted)
							chests[j].inv.remove({ name = req.name, count = inserted })
							return
						end
					end
				end
			end
		end
	end

	if not trash.is_empty() then
		if chests == nil then chests = find_chests_player(p) end

		for item, count in pairs(trash.get_contents()) do
			count = math.min(count, game.item_prototypes[item].stack_size)
			for j = 1, #chests do
				if chests[j].storage == true and chests[j].inv.can_insert({ name = item, count = count }) then
					local inserted = chests[j].inv.insert({ name = item, count = count })
					float_text(p, chests[j].pos, item, -inserted)
					trash.remove({ name = item, count = inserted })
					return
				end
			end
		end
	end
end

local function handle_vehicle(v)
	local requests = {}
	local chests
	local p = v.last_user

	local inv = v.get_inventory(defines.inventory.car_trunk)
	if inv == nil or not inv.is_filtered() then return end

	for i = 1, #inv do
		local filter = inv.get_filter(i)
		if filter ~= nil then
			requests[filter] = (requests[filter] or 0) + game.item_prototypes[filter].stack_size
		end
	end

	local burner = v.burner
	if burner ~= nil then
		local fuel_item
		do
			local cats = burner.fuel_categories
			for i = 1, #inv do
				local filter = inv.get_filter(i)
				if filter ~= nil then
					local cat = game.item_prototypes[filter].fuel_category
					if cat ~= nil and cats[cat] ~= nil then
						fuel_item = filter
						break
					end
				end
			end
		end

		for _, _inv in pairs({ burner.inventory, burner.burnt_result_inventory }) do
			for item, count in pairs(_inv.get_contents()) do
				if item ~= fuel_item then
					count = math.min(count, game.item_prototypes[item].stack_size)

					local in_inv = inv.get_item_count(item)
					if requests[item] ~= nil and requests[item] > in_inv then
						count = math.min(count, requests[item] - in_inv)
						if inv.can_insert({ name = item, count = count }) then
							local inserted = inv.insert({ name = item, count = count })
							float_text(p, v.position, item, inserted)
							_inv.remove({ name = item, count = inserted })
							inv.sort_and_merge()
							return
						end
					end

					if chests == nil then chests = find_chests_vehicle(v) end

					for i = 1, #chests do
						if chests[i].storage == true and chests[i].inv.can_insert({ name = item, count = count }) then
							local inserted = chests[i].inv.insert({ name = item, count = count })
							float_text(p, chests[i].pos, item, -inserted)
							_inv.remove({ name = item, count = inserted })
							return
						end
					end
				end
			end
		end

		if fuel_item ~= nil then
			local fuel = burner.inventory
			local fuel_req = game.item_prototypes[fuel_item].stack_size * #fuel
			local in_fuel = fuel.get_item_count(fuel_item)
			if in_fuel < fuel_req then
				local wanted = math.min(fuel_req - in_fuel, game.item_prototypes[fuel_item].stack_size)

				local in_inv = inv.get_item_count(fuel_item)
				if in_inv > 0 then
					local found = math.min(wanted, in_inv)
					if fuel.can_insert({ name = fuel_item, count = found }) then
						local inserted = fuel.insert({ name = fuel_item, count = found })
						float_text(p, v.position, fuel_item, inserted)
						inv.remove({ name = fuel_item, count = inserted })
						inv.sort_and_merge()
						return
					end
				end

				if chests == nil then chests = find_chests_vehicle(v) end

				for i = 1, #chests do
					local found = math.min(wanted, chests[i].inv.get_item_count(fuel_item))
					if found > 0 and fuel.can_insert({ name = fuel_item, count = found }) then
						local inserted = fuel.insert({ name = fuel_item, count = found })
						float_text(p, chests[i].pos, fuel_item, inserted)
						chests[i].inv.remove({ name = fuel_item, count = inserted })
						return
					end
				end
			end
		end
	end


	local ammo = v.get_inventory(defines.inventory.car_ammo)
	for i, gun_proto in pairs(v.prototype.indexed_guns) do
		local ammo_item
		do
			local cats = {}
			for _, cat in pairs(gun_proto.attack_parameters.ammo_categories) do
				cats[cat] = true
			end
			local max
			for item, count in pairs(requests) do
				local type = game.item_prototypes[item].get_ammo_type("vehicle")
				if type ~= nil then
					local cat = type.category
					if cats[cat] ~= nil then
						if max == nil or count > max[2] then
							max = { item, count }
						end
					end
				end
				if max ~= nil then ammo_item = max[1] end
			end

			if ammo[i].count > 0 and ammo[i].name ~= ammo_item then
				local item, count = ammo[i].name, ammo[i].count

				local in_inv = inv.get_item_count(item)
				if requests[item] ~= nil and requests[item] > in_inv then
					count = math.min(count, requests[item] - in_inv)
					if inv.can_insert({ name = item, count = count }) then
						local inserted = inv.insert({ name = item, count = count })
						float_text(p, v.position, item, inserted)
						util.increment(ammo[i], "count", -inserted)
						inv.sort_and_merge()
						return
					end
				end

				if chests == nil then chests = find_chests_vehicle(v) end

				for j = 1, #chests do
					if chests[j].storage == true and chests[j].inv.can_insert({ name = item, count = count }) then
						local inserted = chests[j].inv.insert({ name = item, count = count })
						float_text(p, chests[j].pos, item, -inserted)
						util.increment(ammo[i], "count", -inserted)
						return
					end
				end
			end

			if ammo_item ~= nil then
				local ammo_req = game.item_prototypes[ammo_item].stack_size
				if ammo[i].count < ammo_req then
					local wanted = ammo_req - ammo[i].count

					local in_inv = inv.get_item_count(ammo_item)
					if in_inv > 0 then
						local found = math.min(wanted, in_inv)
						if ammo.can_insert({ name = ammo_item, count = found }) then
							local inserted = ammo.insert({ name = ammo_item, count = found })
							float_text(p, v.position, ammo_item, inserted)
							inv.remove({ name = ammo_item, count = inserted })
							inv.sort_and_merge()
							return
						end
					end

					if chests == nil then chests = find_chests_vehicle(v) end

					for j = 1, #chests do
						local found = math.min(wanted, chests[j].inv.get_item_count(ammo_item))
						if found > 0 and ammo.can_insert({ name = ammo_item, count = found }) then
							local inserted = ammo.insert({ name = ammo_item, count = found })
							float_text(p, chests[i].pos, ammo_item, inserted)
							chests[i].inv.remove({ name = ammo_item, count = inserted })
							return
						end
					end
				end
			end
		end
	end

	for item, count in pairs(inv.get_contents()) do
		if v.logistic_cell ~= nil and v.logistic_cell.logistic_network ~= nil then
			local network = v.logistic_cell.logistic_network
			if is_construction_bot(item) then
				count = network.all_construction_robots
			elseif game.item_prototypes[item].type == "repair-tool" then
				for _, bot in pairs(network.construction_robots) do
					count = count + bot.get_inventory(defines.inventory.robot_repair).get_item_count(item)
				end
			end
		end

		if requests[item] == nil or count > requests[item] then
			local remove = count
			if requests[item] ~= nil then remove = remove - requests[item] end
			remove = math.min(remove, game.item_prototypes[item].stack_size)

			if chests == nil then chests = find_chests_vehicle(v) end

			for i = 1, #chests do
				if chests[i].storage == true and chests[i].inv.can_insert({ name = item, count = remove }) then
					local inserted = chests[i].inv.insert({ name = item, count = remove })
					float_text(p, chests[i].pos, item, -inserted)
					inv.remove({ name = item, count = inserted })
					inv.sort_and_merge()
					return
				end
			end
		end
	end


	for item, count in pairs(requests) do
		local wanted = count - inv.get_item_count(item)
		if v.logistic_cell ~= nil and v.logistic_cell.logistic_network ~= nil then
			local network = v.logistic_cell.logistic_network
			if is_construction_bot(item) then
				wanted = count - network.all_construction_robots
			elseif game.item_prototypes[item].type == "repair-tool" then
				for _, bot in pairs(network.construction_robots) do
					wanted = wanted - bot.get_inventory(defines.inventory.robot_repair).get_item_count(item)
				end
			end
		end

		if wanted > 0 then
			wanted = math.min(wanted, game.item_prototypes[item].stack_size)

			if chests == nil then chests = find_chests_vehicle(v) end

			for i = 1, #chests do
				local found = math.min(wanted, chests[i].inv.get_item_count(item))
				if found > 0 and inv.can_insert({ name = item, count = found }) then
					local inserted = inv.insert({ name = item, count = found })
					float_text(p, chests[i].pos, item, inserted)
					chests[i].inv.remove({ name = item, count = inserted })
					inv.sort_and_merge()
					return
				end
			end
		end
	end
end

local function tick(e)
	for id, player in pairs(game.players) do
		handle_player(player)
	end

	for _, vehicle in pairs(find_vehicles()) do
		handle_vehicle(vehicle)
	end
end

script.on_init(function()
	global.vehicle_tiles = {}

	script.on_nth_tick(settings.global["issy-el-interval"].value, tick)
end)

script.on_load(function()
	script.on_nth_tick(settings.global["issy-el-interval"].value, tick)

	commands.add_command("rescan-tiles", { "issy.rescan-tiles-description" }, function(c)
		rescan_tiles()
	end)
end)

script.on_configuration_changed(function(data)
	if data.mod_changes["issy-early-logistics"] then
		if v(data.mod_changes["issy-early-logistics"].old_version) <= v("1.1.9") then
			rescan_tiles()
		end
	end

	script.on_nth_tick(nil)
	script.on_nth_tick(settings.global["issy-el-interval"].value, tick)
end)
