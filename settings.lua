data:extend {
	{
		name = "issy-el-interval",
		type = "int-setting",
		setting_type = "runtime-global",
		default_value = 30
	},
	{
		name = "issy-el-range",
		type = "int-setting",
		setting_type = "runtime-global",
		default_value = 0
	},
	{
		name = "issy-el-use-checkbox",
		type = "bool-setting",
		setting_type = "runtime-per-user",
		default_value = true
	}
}
